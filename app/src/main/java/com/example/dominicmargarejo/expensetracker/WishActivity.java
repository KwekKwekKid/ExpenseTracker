package com.example.dominicmargarejo.expensetracker;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.util.UUID;

/**
 * IDK how to explain this. It's important tho???
 */
public class WishActivity extends SingleFragmentActivity{

    public static final String EXTRA_WISH_ID = "com.example.dominicmargarejo.expensetracker.wish_id";

    @Override
    protected Fragment createFragment(){
        UUID wishId = (UUID) getIntent().getSerializableExtra(EXTRA_WISH_ID);
        return WishFragment.newInstance(wishId);
    }

    public static Intent newIntent(Context packageContext, UUID wishId){
        Intent intent = new Intent(packageContext, WishActivity.class);
        intent.putExtra(EXTRA_WISH_ID, wishId);
        return intent;
    }
}
