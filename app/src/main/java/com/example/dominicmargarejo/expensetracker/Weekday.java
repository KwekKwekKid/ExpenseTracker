package com.example.dominicmargarejo.expensetracker;

import java.util.Calendar;

/**
 * Created by Niko on 12/8/2017.
 */

public class Weekday {

    private static String[] daysInString = {"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
    private static int[] daysInInt = {Calendar.SUNDAY, Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY, Calendar.THURSDAY, Calendar.FRIDAY, Calendar.SATURDAY};

    public static String getDayInString(int dayOfWeek){
        for(int i=0; i<7; i++){
            if(daysInInt[i] == dayOfWeek){
                return daysInString[i];
            }
        }
        return "Sunday";
    }

    public static int getDayInInt(String dayOfWeek){
        for(int i=0; i<7; i++){
            if(daysInString[i].equals(dayOfWeek)){
                return daysInInt[i];
            }
        }
        return Calendar.SUNDAY;
    }

}
