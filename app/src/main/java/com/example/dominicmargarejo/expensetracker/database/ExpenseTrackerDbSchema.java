package com.example.dominicmargarejo.expensetracker.database;

/**
 * Created by Niko on 11/4/2017.
 * Edited by Cher on 11/6/2017
 *
 * Defines the Database's tables and stuff
 */

public class ExpenseTrackerDbSchema {

    public static final class TransactionTable{
        public static final String NAME = "transactions";

        public static final class Cols{
            public static final String UUID = "uuid";
            public static final String NAME = "name";
            public static final String CATEGORY = "category";
            public static final String AMOUNT = "amount";
            public static final String DATE = "date";
            public static final String ACCOUNT = "account";
        }
    }

    public static final class AccountTable{
        public static final String NAME = "accounts";

        public static final class Cols{
            public static final String UUID = "uuid";
            public static final String NAME = "name";
            public static final String INFLOW = "inflow";
            public static final String OUTFLOW = "outflow";
            public static final String BALANCE = "balance";
        }
    }

    public static final class AllowanceTable{
        public static final String NAME = "allowance";

        public static final class Cols{
            public static final String UUID = "uuid";
            public static final String NAME = "name";
            public static final String AMOUNT = "amount";
            public static final String ACCOUNT = "account"; //NIKOO added account
            public static final String FREQUENCY = "frequency";//NIKOO
        }
    }

    public static final class UserTable{
        public static final String NAME = "users";

        public static final class Cols{
            public static final String UUID = "uuid";
            public static final String NAME = "name";
            public static final String BIRTHDAY = "birthday";
            public static final String OCCUPATION = "occupation";
            public static final String AMOUNT = "amount"; //derive total amount instead???
        }
    }

    public static final class WishlistTable{
        public static final String NAME = "wishlist";

        public static final class Cols {
            public static final String UUID = "uuid";
            public static final String NAME = "name";
            public static final String PRICE = "price";
            public static final String MAINTAINING = "maintaining";
            public static final String DATE = "date";
            public static final String ACCOUNT = "account";
        }
    }

    public static final class BudgetTable{
        public static final String NAME = "budget";

        public static final class Cols{
            public static final String UUID = "uuid";
            public static final String NAME = "name";
            public static final String AMOUNT = "amount";
        }
    }

    public static final class SavingsTable{
        public static final String NAME = "savings";

        public static final class Cols{
            public static final String UUID = "uuid";
            public static final String DATE = "date";
            public static final String NAME = "name";
            public static final String AMOUNT = "amount";
        }
    }

}