package com.example.dominicmargarejo.expensetracker;

import android.widget.ProgressBar;

import java.util.Date;
import java.util.UUID;

/**
 * Created by cherpanlilio on 11/3/17.
 */

public class Wish {
    private UUID mId;
    private String mWishName;
    private double mWishGoalAmount;
    private double mWishMaintainingAmount;
    private String mWishAccount;
    private Date mWishDate;


    public Wish(){
        mId = UUID.randomUUID();
        mWishDate = new Date();
        mWishName = "";
        mWishAccount = "";
        mWishGoalAmount = 0.00;
        mWishMaintainingAmount = 0.00;
    }

    public Wish(UUID id){
        mId = id;
        mWishDate = new Date();
        mWishName = "";
        mWishAccount = "";
        mWishGoalAmount = 0.00;
        mWishMaintainingAmount = 0.00;
    }

    public double getWishMaintainingAmount() {
        return mWishMaintainingAmount;
    }

    public void setWishMaintainingAmount(double wishMaintainingAmount) {
        mWishMaintainingAmount = wishMaintainingAmount;
    }

    public String getWishName() {
        return mWishName;
    }

    public void setWishName(String wishName) {
        mWishName = wishName;
    }

    public String getWishAccount() {
        return mWishAccount;
    }

    public void setWishAccount(String wishAccount) {
        mWishAccount = wishAccount;
    }

    public double getWishGoalAmount() {
        return mWishGoalAmount;
    }

    public void setWishGoalAmount(double wishGoalAmount) {
        mWishGoalAmount = wishGoalAmount;
    }

    public Date getWishDate() {
        return mWishDate;
    }

    public void setWishDate(Date wishDate) {
        mWishDate = wishDate;
    }

    public UUID getId() {
        return mId;
    }

    public void setId(UUID id) {
        mId = id;
    }
}

