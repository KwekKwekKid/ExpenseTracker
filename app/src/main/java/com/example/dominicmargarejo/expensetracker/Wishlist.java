package com.example.dominicmargarejo.expensetracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerCursorWrapper;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbHelper;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.AccountTable;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.WishlistTable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by cherpanlilio on 11/3/17.
 */

public class Wishlist {

    private static Wishlist sWishlistList;

    private Context mContext;
    private SQLiteDatabase mDatabase;

    public static Wishlist get(Context context){
        if(sWishlistList == null){
            sWishlistList = new Wishlist(context);
        }
        return sWishlistList;
    }

    private Wishlist(Context context){
        mContext = context.getApplicationContext();
        mDatabase = new ExpenseTrackerDbHelper(mContext).getWritableDatabase();
    }

    public void addWish(Wish wish){
        ContentValues values = getContentValues(wish);
        mDatabase.insert(WishlistTable.NAME, null, values);
    }

    public void updateWish(Wish wish){
        String uuidString = wish.getId().toString();
        ContentValues values = getContentValues(wish);
        mDatabase.update(WishlistTable.NAME, values,
                WishlistTable.Cols.UUID + " = ?",
                new String[] {uuidString});
    }

    public void removeWish(Wish wish){
        mDatabase.delete(WishlistTable.NAME, "uuid = ?", new String[]{wish.getId().toString()});
    }

    public List<Wish> getWishlist() {
        List<Wish> wishlist = new ArrayList<>();

        ExpenseTrackerCursorWrapper cursor = queryWishlist(null, null);

        try{
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                wishlist.add(cursor.getWish());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return wishlist;
    }

    public Wish getWish(UUID id){
        ExpenseTrackerCursorWrapper cursor = queryWishlist(
                WishlistTable.Cols.UUID + " = ?",
                new String[] {id.toString()}
        );

        try{
            if(cursor.getCount() == 0){
                return null;
            }
            cursor.moveToFirst();
            return cursor.getWish();
        } finally {
            cursor.close();
        }
    }

    public int indexOf(Wish wish){
        ExpenseTrackerCursorWrapper cursor = queryWishlist(null,null);
        int index = 0;

        try{
            cursor.moveToFirst();
            do{
                Wish tempWishlistItem = cursor.getWish();
                if(wish.getId().equals(tempWishlistItem.getId())) {
                    break;
                }
            } while(cursor.moveToNext());

        } finally {
            index = cursor.getPosition();
            cursor.close();
        }

        return index;
    }

    private static ContentValues getContentValues(Wish wish){
        ContentValues values = new ContentValues();
        values.put(WishlistTable.Cols.UUID, wish.getId().toString());
        values.put(WishlistTable.Cols.NAME, wish.getWishName());
        values.put(WishlistTable.Cols.PRICE, wish.getWishGoalAmount());
        values.put(WishlistTable.Cols.DATE, wish.getWishDate().getTime());
        values.put(WishlistTable.Cols.ACCOUNT, wish.getWishAccount());
        return values;
    }

    private ExpenseTrackerCursorWrapper queryWishlist(String whereClause, String[] whereArgs){
        Cursor cursor = mDatabase.query(
                WishlistTable.NAME,
                null, // columns - null = all columns
                whereClause,
                whereArgs,
                null, //groupBy
                null, // having
                null //orderBy
        );
        return new ExpenseTrackerCursorWrapper(cursor);
    }

}

