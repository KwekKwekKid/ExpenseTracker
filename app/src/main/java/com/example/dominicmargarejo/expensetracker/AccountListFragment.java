package com.example.dominicmargarejo.expensetracker;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dominic Margarejo on 11/3/2017.
 *
 * Fragment for the Account Screen.
 */

public class AccountListFragment extends Fragment {

    //IDK what these do exactly, but they're for maintaining the list.
    private RecyclerView mAccountRecyclerView;
    private AccountAdapter mAdapter;

    //GUI Elements
    private FloatingActionButton mAddButton;
    private EditText mNameField;

    //Other Variables
    private int mModifiedPosition;

    //Added This
    private List<Account> accounts;

    /**
     * Run when the view is created. Set things up here.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        // Added This
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_account_list, container, false);

        mAccountRecyclerView = (RecyclerView) view.findViewById(R.id.account_recycler_view);
        mAccountRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //Button for adding accounts
        mAddButton = (FloatingActionButton) view.findViewById(R.id.add_account_button);
        mAddButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                showDialog("");
            }
        });

        updateUI();

        return view;
    }

    // Added This
    /*@Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem item = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView)item.getActionView(); //getting NullPointer
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                List<Account> temp = new ArrayList<>();
                for (Account a: accounts){
                    String name = a.getAccountName().toLowerCase();
                    if (name.contains(newText)){
                        temp.add(a);
                    }
                }
                mAdapter.setAccounts(temp);
                mAdapter.notifyDataSetChanged();
                return true;
            }
        });
    }*/

    /**
     * Popup Dialog for adding/editing an account. For now editing just means changing the name.
     *
     * @param name the name of the account (blank if new)
     */
    public void showDialog(final String name){
        //This is for making the Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(true);

        //The text field for the dialog and other attributes
        mNameField = new EditText(getActivity());
        mNameField.setHint(R.string.account_name_hint);
        mNameField.setText(name);
        if(!name.equals("")){
            builder.setTitle("Edit Account");
        } else{
            builder.setTitle("New Account");
        }

        builder.setView(mNameField);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String newName = mNameField.getText().toString();
                AccountList accountList = AccountList.get(getActivity());
                //Checks if there is already an existing account (name is not blank)
                if(newName.equals("")){
                    Toast.makeText(getContext(), R.string.account_nameless_error, Toast.LENGTH_SHORT).show();
                } else if(accountList.getAccount(name) != null){
                    Account account = accountList.getAccount(name);
                    Toast.makeText(getContext(), name, Toast.LENGTH_SHORT).show();
                    account.setAccountName(newName);
                    accountList.updateAccount(account);
                    mModifiedPosition = accountList.indexOf(account);//WHY WON'T U WORK
                    updateUI(); //WHY WON'T U WORK
                } else{
                    Account account = new Account();
                    account.setAccountName(newName);
                    accountList.addAccount(account);
                    mModifiedPosition = accountList.indexOf(account);//WHY WON'T U WORK
                    updateUI();//WHY WON'T U WORK
                }

            }
        });

        AlertDialog b = builder.create();
        b.show();
    }

    //Updates the RecyclerView (AKA the list of things)
    private void updateUI() {
        AccountList accountList = AccountList.get(getActivity());
        accounts = accountList.getAccounts();

        if (mAdapter == null) {
            mAdapter = new AccountAdapter(accounts);
            mAccountRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setAccounts(accounts);
            mAdapter.notifyItemChanged(mModifiedPosition);
        }
    }

    //Code for each individual item in the list
    private class AccountHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView mNameTextView;
        private TextView mAmountTextView;
        private TextView mInflowTextView;
        private TextView mOutflowTextView;
        private Account mAccount;

        public AccountHolder(View itemView){
            super(itemView);
            itemView.setOnClickListener(this);
            mNameTextView = (TextView) itemView.findViewById(R.id.list_item_account_name_text_view);
            mAmountTextView = (TextView) itemView.findViewById(R.id.list_item_account_amount_text_view);
            mInflowTextView = (TextView) itemView.findViewById(R.id.account_inflow_text_view);
            mOutflowTextView = (TextView) itemView.findViewById(R.id.account_outflow_text_view);
        }

        public void bindAccount(Account account){
            mAccount = account;
            mNameTextView.setText(mAccount.getAccountName());
            mAmountTextView.setText(String.format("\u20B1 %.2f",mAccount.getAccountInflow() - mAccount.getAccountOutflow()));
            mInflowTextView.setText(String.format("Inflow: \u20B1 %.2f", mAccount.getAccountInflow()));
            mOutflowTextView.setText(String.format("Outflow: \u20B1 %.2f", mAccount.getAccountOutflow()));
        }

        @Override
        public void onClick(View v){
            showDialog(mNameTextView.getText().toString());
        }
    }

    //Ok legit IDK what this is
    private class AccountAdapter extends RecyclerView.Adapter<AccountHolder>{
        private List<Account> mAccounts;

        public AccountAdapter(List<Account> accounts){
            mAccounts = accounts;
        }

        @Override
        public AccountHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_account, parent, false);
            return new AccountHolder(view);
        }

        @Override
        public void onBindViewHolder(AccountHolder holder, int position){
            Account account = mAccounts.get(position);
            holder.bindAccount(account);
        }

        @Override
        public int getItemCount(){
            return mAccounts.size();
        }

        public void setAccounts(List<Account> accounts){
            mAccounts = accounts;
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        updateUI();
    }
}
