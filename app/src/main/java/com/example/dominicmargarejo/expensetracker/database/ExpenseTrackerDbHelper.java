package com.example.dominicmargarejo.expensetracker.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.BudgetTable;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.TransactionTable;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.AccountTable;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.UserTable;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.WishlistTable;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.AllowanceTable;

/**
 * Created by Niko on 11/4/2017.
 *
 * This is the code for creating the database and stuff.
 */

public class ExpenseTrackerDbHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "expensetracker.db";

    public ExpenseTrackerDbHelper(Context context){
        super(context, DATABASE_NAME, null, VERSION);
    }

    /**
     * Only runs when the database is first created.
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db){
        db.execSQL("create table " + TransactionTable.NAME + "(" +
                "_id integer primary key autoincrement, " +
                TransactionTable.Cols.UUID + ", " +
                TransactionTable.Cols.NAME + ", " +
                TransactionTable.Cols.CATEGORY + ", " +
                TransactionTable.Cols.AMOUNT + ", " +
                TransactionTable.Cols.DATE + ", " +
                TransactionTable.Cols.ACCOUNT + ", " +
                "FOREIGN KEY(" + TransactionTable.Cols.ACCOUNT + ") REFERENCES " + AccountTable.NAME + "("+ AccountTable.Cols.NAME + "))"
        );

        db.execSQL("create table " + AccountTable.NAME + "(" +
                "_id integer primary key autoincrement, " +
                AccountTable.Cols.UUID + ", " +
                AccountTable.Cols.NAME + " TEXT, " +
                AccountTable.Cols.INFLOW + " FLOAT(5,2), " +
                AccountTable.Cols.OUTFLOW  + " FLOAT(5,2))"
        );

        db.execSQL("create table " + AllowanceTable.NAME + "(" +
                "_id integer primary key autoincrement, " +
                AllowanceTable.Cols.UUID + ", " +
                AllowanceTable.Cols.NAME + " TEXT, " +
                AllowanceTable.Cols.AMOUNT + " FLOAT(5,2), " +
                AllowanceTable.Cols.FREQUENCY  + ", " + //NIKOO made it not specify the datatype
                AllowanceTable.Cols.ACCOUNT + ", " + //NIKOO added account
                "FOREIGN KEY(" + AllowanceTable.Cols.ACCOUNT + ") REFERENCES " + AccountTable.NAME + "("+ AccountTable.Cols.NAME + "))"
        );

        db.execSQL("create table " + UserTable.NAME + "(" +
                "_id integer primary key autoincrement, " +
                UserTable.Cols.UUID + ", " +
                UserTable.Cols.NAME + ", " +
                UserTable.Cols.BIRTHDAY + ", " +
                UserTable.Cols.OCCUPATION + ", " +
                UserTable.Cols.AMOUNT + ")"
        );

        db.execSQL("create table " + BudgetTable.NAME + "(" +
                "_id integer primary key autoincrement, " +
                BudgetTable.Cols.UUID + ", " +
                BudgetTable.Cols.AMOUNT + ", " +
                BudgetTable.Cols.NAME + ", " +
                "FOREIGN KEY(" + BudgetTable.Cols.NAME + ") REFERENCES " + AccountTable.NAME + "("+ AccountTable.Cols.NAME + "))"
        );

        db.execSQL("create table " + WishlistTable.NAME + "(" +
                "_id integer primary key autoincrement, " +
                WishlistTable.Cols.UUID + ", " +
                WishlistTable.Cols.NAME + ", " +
                WishlistTable.Cols.PRICE + ", " +
                WishlistTable.Cols.MAINTAINING + ", " +
                WishlistTable.Cols.DATE + "," +
                WishlistTable.Cols.ACCOUNT + "," +

                "FOREIGN KEY(" + WishlistTable.Cols.ACCOUNT + ") REFERENCES " + AccountTable.NAME + "("+ AccountTable.Cols.NAME + "))"
        );


    }

    /**
     * Runs when db already exists. Ignore for now. Just delete the app to delete the database. Reinstall and yay new db.
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

    }

}