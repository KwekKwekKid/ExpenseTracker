package com.example.dominicmargarejo.expensetracker;

        import android.content.Context;
        import android.database.Cursor;
        import android.database.sqlite.SQLiteDatabase;
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.TextView;

        import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerCursorWrapper;
        import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbHelper;
        import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema;
        import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.AccountTable;

/**
 * Fragment for the home screen.
 */
public class HomeFragment extends Fragment {

    double mBalance, mInflow, mOutflow;
    TextView mBalanceTextView, mInflowTextView, mOutflowTextView;
    SQLiteDatabase mDatabase;
    Context mContext;
    ExpenseTrackerDbHelper expenseTrackerDbHelper;
    Cursor balanceCursor, inflowCursor, outflowCursor;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        mContext = getContext();
        mDatabase = new ExpenseTrackerDbHelper(mContext).getWritableDatabase();

        mBalanceTextView = view.findViewById(R.id.home_balance);
        mInflowTextView = view.findViewById(R.id.home_inflow);
        mOutflowTextView = view.findViewById(R.id.home_outflow);

        Cursor cursor = mDatabase.rawQuery("SELECT SUM(" + AccountTable.Cols.INFLOW + ") FROM " + AccountTable.NAME , null);
        if(cursor.moveToFirst())
        {
            mInflow = cursor.getInt(0);
        } else{
            mInflow = 0.00;
        }

        cursor = mDatabase.rawQuery("SELECT SUM(" + AccountTable.Cols.OUTFLOW + ") FROM " + AccountTable.NAME , null);;
        if(cursor.moveToFirst())
        {
            mOutflow = cursor.getInt(0);
        } else{
            mOutflow = 0.00;
        }

        mBalance = mInflow - mOutflow;

        String prefix = " \u20b1";
        if(mBalance < 0){
            prefix = " - \u20b1 ";
        }
        mBalanceTextView.setText(String.format(prefix + "%.2f", mBalance));
        mInflowTextView.setText(String.format(prefix + "%.2f", mInflow));
        mOutflowTextView.setText(String.format(prefix + "%.2f", mOutflow));
        return view;
    }

/**
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Home");
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }
**/
}
