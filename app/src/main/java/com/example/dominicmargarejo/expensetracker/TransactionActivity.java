package com.example.dominicmargarejo.expensetracker;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.util.UUID;

/**
 * IDK how to explain this. It's important tho???
 */
public class TransactionActivity extends SingleFragmentActivity{

    public static final String EXTRA_TRANSACTION_ID = "com.example.dominicmargarejo.expensetracker.transaction_id";

    @Override
    protected Fragment createFragment(){
        UUID transactionId = (UUID) getIntent().getSerializableExtra(EXTRA_TRANSACTION_ID);
        return TransactionFragment.newInstance(transactionId);
    }

    public static Intent newIntent(Context packageContext, UUID transactionId){
        Intent intent = new Intent(packageContext, TransactionActivity.class);
        intent.putExtra(EXTRA_TRANSACTION_ID, transactionId);
        return intent;
    }
}
