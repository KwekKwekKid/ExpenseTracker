package com.example.dominicmargarejo.expensetracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dominic Margarejo on 12/7/2017.
 */

public class AllowanceListFragment extends Fragment {

    private static final String MODIFIED_POSITION = "modified_position";
    private static final int REQUEST_POSITION = 1;
    private int mModifiedPosition = 0;
    private boolean deleteSelected = false;

    private RecyclerView mAllowanceRecyclerView;
    private AllowanceAdapter mAdapter;
    // Added This
    private List<Allowance> allowances;

    private boolean FABExpanded;
    private FloatingActionButton mEditButton;
    private FloatingActionButton mAddButton;
    private FloatingActionButton mDeleteButton;


    public void updateAdapter(){
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        // Added This
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_allowance_list, container, false);

        mAllowanceRecyclerView = (RecyclerView) view.findViewById(R.id.allowance_recycler_view);
        mAllowanceRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        FABExpanded = false;

        mEditButton = (FloatingActionButton) view.findViewById(R.id.allowance_open_actions_button);
        mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (FABExpanded){
                    mAddButton.setVisibility(View.GONE);
                    mDeleteButton.setVisibility(View.GONE);
                    FABExpanded = false;
                } else {
                    mAddButton.setVisibility(View.VISIBLE);
                    mDeleteButton.setVisibility(View.VISIBLE);
                    FABExpanded = true;
                }
            }
        });

        mAddButton = (FloatingActionButton) view.findViewById(R.id.add_allowance_button);
        mAddButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mAddButton.setVisibility(View.GONE);
                mDeleteButton.setVisibility(View.GONE);
                Allowance allowance = new Allowance();
                AllowanceList.get(getContext()).addAllowance(allowance);
                Intent intent = AllowanceActivity.newIntent(getContext(), allowance.getId());
                startActivityForResult(intent, REQUEST_POSITION);
            }
        });

        //doesn't really do anything yet
        mDeleteButton = (FloatingActionButton) view.findViewById(R.id.remove_allowance_button);
        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteSelected = true;
                updateAdapter();
                deleteSelected = false;

            }
        });

        updateUI();

        return view;
    }

    // Added This
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem item = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                List<Allowance> temp = new ArrayList<>();
                for (Allowance a: allowances){
                    String name = a.getAllowanceName().toLowerCase();
                    if (name.contains(newText)){
                        temp.add(a);
                    }
                }
                mAdapter.setAllowances(temp);
                updateAdapter();
                return true;
            }
        });
    }

    private void updateUI() {
        AllowanceList allowanceList = AllowanceList.get(getActivity());
        allowances = allowanceList.getAllowances();

        if (mAdapter == null) {
            mAdapter = new AllowanceAdapter(allowances,this);
            mAllowanceRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setAllowances(allowances);
            mAdapter.notifyItemChanged(mModifiedPosition);
        }
    }

    private class AllowanceHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView mNameTextView;
        private TextView mAmountTextView;
        private TextView mAccountTextView;
        private TextView mFrequencyTextView;
        private TextView mProgressTextView;
        private ProgressBar mProgressBar;
        private Allowance mAllowance;

        private AllowanceListFragment mAllowanceListFragment;

        public AllowanceHolder(View itemView, AllowanceListFragment allowanceListFragment){
            super(itemView);

            mAllowanceListFragment = allowanceListFragment;

            mNameTextView = (TextView) itemView.findViewById(R.id.list_item_allowance_name_text_view);
            mAmountTextView = (TextView) itemView.findViewById(R.id.list_item_allowance_amount_text_view);
            mFrequencyTextView = (TextView) itemView.findViewById(R.id.list_item_allowance_frequency_text_view);
            mAccountTextView = (TextView) itemView.findViewById(R.id.list_item_allowance_account_text_view);
            mProgressTextView = (TextView) itemView.findViewById(R.id.allowance_progress_text);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.allowance_progress_bar);

            itemView.setOnClickListener(this);

        }

        public void bindAllowance(Allowance allowance){
            mAllowance = allowance;
            mNameTextView.setText(mAllowance.getAllowanceName());
            mAmountTextView.setText(String.format("%.2f",mAllowance.getAllowanceAmount()));
            mAccountTextView.setText(mAllowance.getAllowanceAccount());


            mFrequencyTextView.setText("Frequency: Every " + Weekday.getDayInString(mAllowance.getAllowanceFrequency()));


            Calendar currentDate = Calendar.getInstance();
            int currentDay = currentDate.get(Calendar.DAY_OF_WEEK);
            int goalDay = mAllowance.getAllowanceFrequency();
            int daysLeft = 0;
            while(currentDay != goalDay){
                currentDate.add(Calendar.DAY_OF_WEEK, 1);
                currentDay = currentDate.get(Calendar.DAY_OF_WEEK);
                daysLeft++;
            }

            Transaction transaction = TransactionList.get(getContext()).getRecentTransaction(mAllowance.getAllowanceName());
            if(daysLeft == 0){

                Transaction newTransaction = mAllowance.allowanceTransaction();

                if(transaction == null){
                    AccountList accounts = AccountList.get(getContext());
                    Account updatedAccount = accounts.getAccount(mAllowance.getAllowanceAccount());
                    updatedAccount.setAccountInflow(updatedAccount.getAccountInflow() + mAllowance.getAllowanceAmount());
                    accounts.updateAccount(updatedAccount);
                    TransactionList.get(getContext()).addTransaction(newTransaction);
                }
                else {
                    Calendar tempCalendar = Calendar.getInstance();
                    tempCalendar.setTime(transaction.getTransactionDate());
                    if(tempCalendar.get(Calendar.DAY_OF_WEEK_IN_MONTH) != currentDate.get(Calendar.DAY_OF_WEEK_IN_MONTH)) {
                        AccountList accounts = AccountList.get(getContext());
                        Account updatedAccount = accounts.getAccount(newTransaction.getTransactionAccount());
                        updatedAccount.setAccountInflow(updatedAccount.getAccountInflow() + mAllowance.getAllowanceAmount());
                        accounts.updateAccount(updatedAccount);
                        TransactionList.get(getContext()).addTransaction(newTransaction);
                    }
                }

            }

            int progress = (int) Math.round( (7-daysLeft)/7.0 * 100.0) ;
            System.out.println(progress);
            mProgressBar.setProgress(progress);

            mProgressTextView.setText( Integer.toString( daysLeft ) + " days left" );
        }

        @Override
        public void onClick(View v){
            Intent intent = AllowanceActivity.newIntent(getActivity(), mAllowance.getId());
            startActivityForResult(intent, REQUEST_POSITION);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_POSITION){
            if(resultCode == Activity.RESULT_OK){
                mModifiedPosition = data.getIntExtra(MODIFIED_POSITION, 0);
            }
        }
    }

    private class AllowanceAdapter extends RecyclerView.Adapter<AllowanceHolder> {
        private List<Allowance> mAllowances;
        private AllowanceListFragment mAllowanceListFragment;

        public AllowanceAdapter(List<Allowance> allowances, AllowanceListFragment allowanceListFragment){
            mAllowances = allowances;
            mAllowanceListFragment = allowanceListFragment;
        }

        @Override
        public AllowanceHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_allowance, parent, false);
            return new AllowanceHolder(view, mAllowanceListFragment);
        }

        @Override
        public void onBindViewHolder(AllowanceHolder holder, int position){
            Allowance allowance = mAllowances.get(position);
            holder.bindAllowance(allowance);
        }

        @Override
        public int getItemCount(){
            return mAllowances.size();
        }

        public void setAllowances(List<Allowance> allowances){
            mAllowances = allowances;
        }

    }

    @Override
    public void onResume(){
        super.onResume();
        updateUI();
    }
}
