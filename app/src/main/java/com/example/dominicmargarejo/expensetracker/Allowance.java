package com.example.dominicmargarejo.expensetracker;

import java.util.Calendar;
import java.util.UUID;

/**
 * Created by Dominic Margarejo on 12/7/2017.
 */

public class Allowance {
    private UUID mId; //This is not gonna be displayed, but it's for searching through the database.
    private String mAllowanceName = "";
    private double mAllowanceAmount = 0.00;
    private int mAllowanceFrequency = Calendar.DAY_OF_WEEK; //INTEGER OF DAY OF WEEK
    private String mAllowanceAccount = "";

    public String getAllowanceAccount() {
        return mAllowanceAccount;
    }

    public void setAllowanceAccount(String allowanceAccount) {
        mAllowanceAccount = allowanceAccount;
    }

    public Allowance(){
        mId = UUID.randomUUID();
    }

    public Allowance(UUID id){
        mId = id;
    }

    public String getAllowanceName() {
        return mAllowanceName;
    }

    public void setAllowanceName(String AllowanceName) {
        mAllowanceName = AllowanceName;
    }

    public UUID getId() {
        return mId;
    }

    public void setId(UUID id) {
        mId = id;
    }

    public double getAllowanceAmount() {
        return mAllowanceAmount;
    }

    public void setAllowanceAmount(double allowanceAmount) {
        mAllowanceAmount = allowanceAmount;
    }

    public int getAllowanceFrequency() {
        return mAllowanceFrequency;
    }

    public void setAllowanceFrequency(int allowanceFrequency) {
        mAllowanceFrequency = allowanceFrequency;
    }

    public Transaction allowanceTransaction(){
        Transaction transaction = new Transaction();
        transaction.setTransactionName(getAllowanceName());
        transaction.setTransactionAccount(getAllowanceAccount());
        transaction.setTransactionAmount(getAllowanceAmount());
        transaction.setTransactionCategory("School"); //FIX LATER
        return transaction;
    }
}
