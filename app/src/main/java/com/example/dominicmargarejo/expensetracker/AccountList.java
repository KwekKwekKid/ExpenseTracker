package com.example.dominicmargarejo.expensetracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerCursorWrapper;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbHelper;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.AccountTable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dominic Margarejo on 11/3/2017.
 *
 * Stores all the accounts
 *
 */

public class AccountList {

    private static AccountList sAccountList; // used to only have one instance of an AccountList

    private Context mContext; //IDK what context means, but ya it's important I guess
    private SQLiteDatabase mDatabase; // The database

    /**
     * Used to get the AccountList. Call AccountList.get(context).
     * @param context the current context
     * @return The AccountList
     */
    public static AccountList get(Context context){
        if(sAccountList == null){
            sAccountList = new AccountList(context);
        }
        return sAccountList;
    }

    private AccountList(Context context){
        mContext = context.getApplicationContext();
        mDatabase = new ExpenseTrackerDbHelper(mContext).getWritableDatabase();
    }

    public void addAccount(Account account){
        ContentValues values = getContentValues(account);
        mDatabase.insert(AccountTable.NAME, null, values);
    }

    public void updateAccount(Account account){
        String uuidString = account.getId().toString();
        ContentValues values = getContentValues(account);
        mDatabase.update(AccountTable.NAME, values,
                AccountTable.Cols.UUID + " = ?",
                new String[] {uuidString});
    }

    public void removeAccount(Account account){
        mDatabase.delete(AccountTable.NAME, "uuid = ?", new String[]{account.getId().toString()});
    }

    public List<Account> getAccounts() {
        List<Account> accounts = new ArrayList<>();

        ExpenseTrackerCursorWrapper cursor = queryAccounts(null, null);

        try{
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                accounts.add(cursor.getAccount());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return accounts;
    }

    public List<String> getAccountNames(){
        List<String> accounts = new ArrayList<>();

        String selectQuery = "SELECT " + AccountTable.Cols.NAME + " FROM " + AccountTable.NAME;

        ExpenseTrackerCursorWrapper cursor = queryAccounts(null,null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                accounts.add(cursor.getString(cursor.getColumnIndex(AccountTable.Cols.NAME)));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();

        return accounts;
    }

    public Account getAccount(String name){
        ExpenseTrackerCursorWrapper cursor = queryAccounts(
                AccountTable.Cols.NAME + " = ?",
                new String[] {name}
        );

        try{
            if(cursor.getCount() == 0){
                return null;
            }
            cursor.moveToFirst();
            return cursor.getAccount();
        } finally {
            cursor.close();
        }
    }

    public int indexOf(Account account){
        ExpenseTrackerCursorWrapper cursor = queryAccounts(null,null);
        int index = 0;

        try{
            cursor.moveToFirst();
            do{
                Account tempAccount = cursor.getAccount();
                if(account.getId().equals(tempAccount.getId())) {
                    break;
                }
            } while(cursor.moveToNext());

        } finally {
            index = cursor.getPosition();
            cursor.close();
        }

        return index;
    }

    /**
     * Puts the values of the account in a format the database can read.
     *
     * @param account the account to get values from
     * @return the ContentValues to use
     */
    private static ContentValues getContentValues(Account account){
        ContentValues values = new ContentValues();
        values.put(AccountTable.Cols.UUID, account.getId().toString());
        values.put(AccountTable.Cols.NAME, account.getAccountName());
        values.put(AccountTable.Cols.INFLOW, account.getAccountInflow());
        values.put(AccountTable.Cols.OUTFLOW, account.getAccountOutflow());

        return values;
    }

    /**
     * Goes through the accounts table in the database
     *
     * @param whereClause Where you're looking, like "NAME = ?"
     * @param whereArgs Specific values like "Jimbob"
     * @return
     */
    private ExpenseTrackerCursorWrapper queryAccounts(String whereClause, String[] whereArgs){
        Cursor cursor = mDatabase.query(
                AccountTable.NAME,
                null, // columns - null = all columns
                whereClause,
                whereArgs,
                null, //groupBy
                null, // having
                null //orderBy
        );

        return new ExpenseTrackerCursorWrapper(cursor);
    }
}
