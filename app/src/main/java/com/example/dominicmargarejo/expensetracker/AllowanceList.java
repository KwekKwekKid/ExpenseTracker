package com.example.dominicmargarejo.expensetracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerCursorWrapper;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbHelper;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.AllowanceTable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Dominic Margarejo on 12/7/2017.
 */

public class AllowanceList {

    private static AllowanceList sAllowanceList; // used to only have one instance of an AllowanceList

    private Context mContext; //IDK what context means, but ya it's important I guess
    private SQLiteDatabase mDatabase; // The database

    /**
     * Used to get the AllowanceList. Call AllowanceList.get(context).
     * @param context the current context
     * @return The AllowanceList
     */
    public static AllowanceList get(Context context){
        if(sAllowanceList == null){
            sAllowanceList = new AllowanceList(context);
        }
        return sAllowanceList;
    }

    private AllowanceList(Context context){
        mContext = context.getApplicationContext();
        mDatabase = new ExpenseTrackerDbHelper(mContext).getWritableDatabase();
    }

    public void addAllowance(Allowance allowance){
        ContentValues values = getContentValues(allowance);
        mDatabase.insert(AllowanceTable.NAME, null, values);
    }

    public void updateAllowance(Allowance allowance){
        String uuidString = allowance.getId().toString();
        ContentValues values = getContentValues(allowance);
        mDatabase.update(AllowanceTable.NAME, values,
                AllowanceTable.Cols.UUID + " = ?",
                new String[] {uuidString});
    }

    public void removeAllowance(Allowance allowance){
        mDatabase.delete(AllowanceTable.NAME, "uuid = ?", new String[]{allowance.getId().toString()});
    }

    public List<Allowance> getAllowances() {
        List<Allowance> allowances = new ArrayList<>();

        ExpenseTrackerCursorWrapper cursor = queryAllowances(null, null);

        try{
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                allowances.add(cursor.getAllowance());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return allowances;
    }

    public List<String> getAllowanceNames(){
        List<String> allowances = new ArrayList<>();

        String selectQuery = "SELECT " + AllowanceTable.Cols.NAME + " FROM " + AllowanceTable.NAME;

        ExpenseTrackerCursorWrapper cursor = queryAllowances(null,null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                allowances.add(cursor.getString(cursor.getColumnIndex(AllowanceTable.Cols.NAME)));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();

        return allowances;
    }

    public Allowance getAllowance(UUID id){
        ExpenseTrackerCursorWrapper cursor = queryAllowances(
                AllowanceTable.Cols.UUID + " = ?",
                new String[] {id.toString()}
        );

        try{
            if(cursor.getCount() == 0){
                return null;
            }
            cursor.moveToFirst();
            return cursor.getAllowance();
        } finally {
            cursor.close();
        }
    }

    public int indexOf(Allowance allowance){
        ExpenseTrackerCursorWrapper cursor = queryAllowances(null,null);
        int index = 0;

        try{
            cursor.moveToFirst();
            do{
                Allowance tempAllowance = cursor.getAllowance();
                if(allowance.getId().equals(tempAllowance.getId())) {
                    break;
                }
            } while(cursor.moveToNext());

        } finally {
            index = cursor.getPosition();
            cursor.close();
        }

        return index;
    }

    /**
     * Puts the values of the Allowance in a format the database can read.
     *
     * @param allowance the Allowance to get values from
     * @return the ContentValues to use
     */
    private static ContentValues getContentValues(Allowance allowance){
        ContentValues values = new ContentValues();
        values.put(AllowanceTable.Cols.UUID, allowance.getId().toString());
        values.put(AllowanceTable.Cols.NAME, allowance.getAllowanceName());
        values.put(AllowanceTable.Cols.AMOUNT, allowance.getAllowanceAmount());
        values.put(AllowanceTable.Cols.ACCOUNT, allowance.getAllowanceAccount());
        values.put(AllowanceTable.Cols.FREQUENCY, allowance.getAllowanceFrequency());

        return values;
    }

    /**
     * Goes through the Allowances table in the database
     *
     * @param whereClause Where you're looking, like "NAME = ?"
     * @param whereArgs Specific values like "Jimbob"
     * @return
     */
    private ExpenseTrackerCursorWrapper queryAllowances(String whereClause, String[] whereArgs){
        Cursor cursor = mDatabase.query(
                AllowanceTable.NAME,
                null, // columns - null = all columns
                whereClause,
                whereArgs,
                null, //groupBy
                null, // having
                null //orderBy
        );

        return new ExpenseTrackerCursorWrapper(cursor);
    }
}
