package com.example.dominicmargarejo.expensetracker;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import java.util.UUID;

/**
 * Created by Dominic Margarejo on 12/7/2017.
 */

public class AllowanceActivity extends SingleFragmentActivity{

    public static final String EXTRA_ALLOWANCE_ID = "com.example.dominicmargarejo.expensetracker.allowance_id";

    @Override
    protected Fragment createFragment(){
        UUID allowanceId = (UUID) getIntent().getSerializableExtra(EXTRA_ALLOWANCE_ID);
        return AllowanceFragment.newInstance(allowanceId);
    }

    public static Intent newIntent(Context packageContext, UUID allowanceId){
        Intent intent = new Intent(packageContext, AllowanceActivity.class);
        intent.putExtra(EXTRA_ALLOWANCE_ID, allowanceId);
        return intent;
    }
}
