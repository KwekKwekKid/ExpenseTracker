package com.example.dominicmargarejo.expensetracker;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerCursorWrapper;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbHelper;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.AccountTable;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Niko on 10/20/2017.
 *
 * Fragment for adding individual transactions.
 */

public class TransactionFragment extends Fragment {
    private Transaction mTransaction;

    private EditText mNameField;
    private EditText mAmountField;

    private Spinner mCategorySpinner;
    private Spinner mAccountSpinner;

    private Button mDateButton;
    private Button mSubmitButton;
    private Button mCancelButton;
    private Button mDeleteButton;

    private ToggleButton mToggleButton;

    //Used for determining whether to add or edit a transaction
    private boolean isNew;

    private String newName;
    private double originalMultiplier;
    private double amountMultiplier;
    private double newAmount;
    private double originalAmount;
    private String originalAccount;
    private String newCategory;
    private String newAccount;

    private SQLiteDatabase mDatabase;

    private Date newDate;

    private static final String ARG_TRANSACTION_ID = "transaction_id";
    private static final String MODIFIED_POSITION = "modified_position";
    private static final int REQUEST_DATE = 0;
    private static final String DIALOG_DATE = "DialogDate";

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        UUID transactionId = (UUID) getArguments().getSerializable(ARG_TRANSACTION_ID);
        mTransaction = TransactionList.get(getActivity()).getTransaction(transactionId);

        newName = mTransaction.getTransactionName();
        newDate = mTransaction.getTransactionDate();

        newAmount = mTransaction.getTransactionAmount();
        if(newAmount <= 0){ amountMultiplier = -1;}
        else { amountMultiplier = 1; }
        newAmount = Math.abs(newAmount);
        originalAmount = newAmount;
        originalMultiplier = amountMultiplier;

        mDatabase = new ExpenseTrackerDbHelper(getActivity().getApplicationContext()).getWritableDatabase();

        newCategory = mTransaction.getTransactionCategory();
        newAccount = mTransaction.getTransactionAccount();
        originalAccount = newAccount;

        isNew = newName.equals("");

        if(isNew) { TransactionList.get(getActivity()).removeTransaction(mTransaction); }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_DATE) {
            Date date = (Date) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            newDate = date;
            mDateButton.setText(new DateFormat().getLongDateFormat(this.getContext()).format(newDate));
        }
    }

    //Used for determining which position in the list to update. I think. ???
    public void returnResult(int result){
        Intent data = new Intent();
        if(result != Activity.RESULT_CANCELED) {
            data.putExtra(MODIFIED_POSITION, TransactionList.get(getActivity()).indexOf(mTransaction));
            System.out.println(TransactionList.get(getActivity()).indexOf(mTransaction));
        }
        getActivity().setResult(result, data);
        getActivity().finish();
    }

    @Override
    public void onPause(){
        super.onPause();
        TransactionList.get(getActivity()).updateTransaction(mTransaction);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_transaction, container, false);

        mNameField = (EditText) v.findViewById(R.id.transaction_name);
        mNameField.setText(mTransaction.getTransactionName());
        mNameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                newName = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mToggleButton = (ToggleButton) v.findViewById(R.id.transaction_toggle_button);
        if(amountMultiplier == -1) { mToggleButton.setChecked(false); }
        else { mToggleButton.setChecked(true); }
        mToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                amountMultiplier *= -1;
            }
        });

        mAmountField = (EditText) v.findViewById(R.id.transaction_amount);
        mAmountField.setText(Double.toString(newAmount));
        mAmountField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(s)){
                    newAmount = 0.0;
                }
                else {
                   newAmount = Double.parseDouble(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mCategorySpinner = (Spinner) v.findViewById(R.id.category_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.category_list, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCategorySpinner.setAdapter(adapter);
        mCategorySpinner.setSelection(adapter.getPosition(newCategory));
        mCategorySpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id){
                newCategory = parent.getItemAtPosition(pos).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){

            }
        });

        List<String> accounts = AccountList.get(getContext()).getAccountNames();
        mAccountSpinner = (Spinner) v.findViewById(R.id.account_spinner);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, accounts);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mAccountSpinner.setAdapter(adapter2);
        mAccountSpinner.setSelection(adapter2.getPosition(newAccount));
        mAccountSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id){
                newAccount = parent.getItemAtPosition(pos).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){

            }
        });

        mDateButton = (Button) v.findViewById(R.id.transaction_date);
        mDateButton.setText(new DateFormat().getLongDateFormat(this.getContext()).format(mTransaction.getTransactionDate()));
        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getFragmentManager();
                DatePickerFragment dialog = DatePickerFragment.newInstance(newDate);
                dialog.setTargetFragment(TransactionFragment.this, REQUEST_DATE);
                dialog.show(manager, DIALOG_DATE);
            }
        });

        mSubmitButton = (Button) v.findViewById(R.id.transaction_submit_button);
        mSubmitButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                AccountList accountList = AccountList.get(getContext());

                if(newName.equals("")){
                    Toast.makeText(getContext(), R.string.transaction_nameless_error, Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    mTransaction.setTransactionName(newName);
                    mTransaction.setTransactionAmount(newAmount * amountMultiplier);
                    mTransaction.setTransactionCategory(newCategory);
                    mTransaction.setTransactionAccount(newAccount);
                    mTransaction.setTransactionDate(newDate);

                    if(isNew){ TransactionList.get(getActivity()).addTransaction(mTransaction); }

                    if(!originalAccount.equals("") && originalAccount != newAccount){
                        Account oldAccount = accountList.getAccount(originalAccount);
                        double amount = originalAmount * originalMultiplier;
                        if(amount < 0){
                            oldAccount.setAccountOutflow(oldAccount.getAccountOutflow() - originalAmount);
                        } else if (amount > 0){
                            oldAccount.setAccountInflow(oldAccount.getAccountInflow() - originalAmount);
                        }
                        AccountList.get(getContext()).updateAccount(oldAccount);
                    }

                    Account updatedAccount = accountList.getAccount(newAccount);
                    if(mTransaction.getTransactionAmount() < 0){
                        updatedAccount.setAccountOutflow(updatedAccount.getAccountOutflow() + newAmount);
                        if(originalMultiplier != amountMultiplier){
                            updatedAccount.setAccountInflow(updatedAccount.getAccountInflow() - originalAmount);
                        }
                    } else if (mTransaction.getTransactionAmount() > 0){
                        updatedAccount.setAccountInflow(updatedAccount.getAccountInflow() + newAmount);
                        if(originalMultiplier != amountMultiplier){
                            updatedAccount.setAccountOutflow(updatedAccount.getAccountOutflow() - originalAmount);
                        }
                    }
                    accountList.updateAccount(updatedAccount);
                }

                returnResult(Activity.RESULT_OK);
            }
        });

        mCancelButton = (Button) v.findViewById(R.id.transaction_cancel_button);
        mCancelButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                returnResult(Activity.RESULT_CANCELED);
            }
        });

        mDeleteButton = (Button) v.findViewById(R.id.transaction_delete_button);
        mDeleteButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

            }
        });

        return v;
    }

    public static TransactionFragment newInstance(UUID transactionId){
        Bundle args = new Bundle();
        args.putSerializable(ARG_TRANSACTION_ID, transactionId);
        TransactionFragment fragment = new TransactionFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
