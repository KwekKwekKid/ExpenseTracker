package com.example.dominicmargarejo.expensetracker;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbHelper;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Niko on 10/20/2017.
 *
 * Fragment for adding individual wish.
 */

public class WishFragment extends Fragment {

    private EditText mNameField;
    private EditText mCostField;
    private EditText mMaintainingField;
    private Spinner mAccountSpinner;
    private Button mDateButton;
    private Button mSubmitButton;
    private Button mCancelButton;
    private Button mPurchaseButton;

    private String newName;
    private double newCost;
    private double newMaintaining;
    private String newAccount;
    private Date newDate;
    private SQLiteDatabase mDatabase;

    private boolean isNew;

    private Wish mWish;

    private Spinner mCategorySpinner;
    private String transactionCategory;

    private static final String ARG_WISH_ID = "wish_id";
    private static final String DIALOG_DATE = "DialogDate";
    private static final String MODIFIED_POSITION = "modified_position";
    private static final int REQUEST_DATE = 0;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        UUID wishId = (UUID) getArguments().getSerializable(ARG_WISH_ID);
        mWish = Wishlist.get(getActivity()).getWish(wishId);

        newName = mWish.getWishName();
        newDate= mWish.getWishDate();
        newAccount = mWish.getWishAccount();
        newCost = mWish.getWishGoalAmount();
        newMaintaining = mWish.getWishMaintainingAmount();
        mDatabase = new ExpenseTrackerDbHelper(getActivity().getApplicationContext()).getWritableDatabase();

        isNew = newName.equals("");

        if(isNew) { Wishlist.get(getActivity()).removeWish(mWish); }
    }

    //Used for determining which position in the list to update. I think. ???
    public void returnResult(int result){
        Intent data = new Intent();
        if(result != Activity.RESULT_CANCELED) {
            data.putExtra(MODIFIED_POSITION, Wishlist.get(getActivity()).indexOf(mWish));
        }
        getActivity().setResult(result, data);
        getActivity().finish();
    }

    @Override
    public void onPause(){
        super.onPause();
        Wishlist.get(getActivity()).updateWish(mWish);
    }

    /**
     * Popup Dialog for adding/editing an account. For now editing just means changing the name.
     *
     */
    public void showDialog(){
        //This is for making the Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Purchase Wish");
        builder.setCancelable(true);

        //The text field for the dialog and other attributes
        mCategorySpinner = new Spinner(getActivity());
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.category_list, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCategorySpinner.setAdapter(adapter);
        mCategorySpinner.setSelection(adapter.getPosition(transactionCategory));
        mCategorySpinner.setPadding(4,4,4,4);
        mCategorySpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id){
                transactionCategory = parent.getItemAtPosition(pos).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){

            }
        });

        TextView title = new TextView(getContext());
        title.setText("Category");
        title.setPadding(4,4,4,4);

        LinearLayout layout = new LinearLayout(getContext());
        layout.addView(title);
        layout.addView(mCategorySpinner);

        builder.setView(layout);

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Account account = AccountList.get(getContext()).getAccount(newAccount);
                Transaction wishTransaction = new Transaction();
                wishTransaction.setTransactionAccount(newAccount);
                wishTransaction.setTransactionAmount(-newCost);
                wishTransaction.setTransactionCategory(transactionCategory);
                wishTransaction.setTransactionName(newName);

                account.setAccountOutflow(account.getAccountOutflow() + newCost);
                AccountList.get(getContext()).updateAccount(account);

                TransactionList.get(getActivity()).addTransaction(wishTransaction);

                Wishlist.get(getActivity()).removeWish(mWish);

                returnResult(Activity.RESULT_CANCELED);

            }
        });

        AlertDialog b = builder.create();
        b.show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_wish, container, false);

        mNameField = (EditText) v.findViewById(R.id.wish_name);
        mNameField.setText(newName);
        mNameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                newName = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mCostField = (EditText) v.findViewById(R.id.wish_amount);
        mCostField.setText(Double.toString(newCost));
        mCostField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(s)){
                    newCost = 0.0;
                }
                else {
                    newCost = Double.parseDouble(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mMaintainingField = (EditText) v.findViewById(R.id.wish_maintaining);
        mMaintainingField.setText(Double.toString(newMaintaining));
        mMaintainingField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(s)){
                    newMaintaining = 0.0;
                }
                else {
                    newMaintaining = Double.parseDouble(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        List<String> accounts = AccountList.get(getContext()).getAccountNames();
        mAccountSpinner = (Spinner) v.findViewById(R.id.wish_account);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, accounts);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mAccountSpinner.setAdapter(adapter);
        mAccountSpinner.setSelection(adapter.getPosition(newAccount));
        mAccountSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id){
                newAccount = parent.getItemAtPosition(pos).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){

            }
        });

        mDateButton = (Button) v.findViewById(R.id.wish_date);
        mDateButton.setText(new DateFormat().getLongDateFormat(this.getContext()).format(mWish.getWishDate()));
        mDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getFragmentManager();
                DatePickerFragment dialog = DatePickerFragment.newInstance(newDate);
                dialog.setTargetFragment(WishFragment.this, REQUEST_DATE);
                dialog.show(manager, DIALOG_DATE);
            }
        });

        mSubmitButton = (Button) v.findViewById(R.id.wish_submit_button);
        mSubmitButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                Wishlist wishlist= Wishlist.get(getContext());

                if(newName.equals("")){
                    Toast.makeText(getContext(), R.string.wish_nameless_error, Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    mWish.setWishName(newName);
                    mWish.setWishGoalAmount(newCost);
                    mWish.setWishMaintainingAmount(newMaintaining);
                    mWish.setWishDate(newDate);
                    mWish.setWishAccount(newAccount);
                    if(isNew){wishlist.addWish(mWish); }
                }

                returnResult(Activity.RESULT_OK);
            }
        });

        mCancelButton = (Button) v.findViewById(R.id.wish_cancel_button);
        mCancelButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                returnResult(Activity.RESULT_CANCELED);
            }
        });

        mPurchaseButton = (Button) v.findViewById(R.id.wish_purchase_button);
        mPurchaseButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Account account = AccountList.get(getContext()).getAccount(newAccount);
                double balance = account.getAccountInflow() - account.getAccountOutflow() - newMaintaining;
                if(newName.equals("")){
                    Toast.makeText(getContext(), R.string.wish_nameless_error, Toast.LENGTH_SHORT).show();
                } else if(balance < newCost){
                    String message = "You are missing " + String.format("\u20B1%.2f",newCost - balance) ;
                    Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                } else{
                    showDialog();
                }
            }
        });


        return v;
    }

    public static WishFragment newInstance(UUID wishId){
        Bundle args = new Bundle();
        args.putSerializable(ARG_WISH_ID, wishId);
        WishFragment fragment = new WishFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(resultCode != Activity.RESULT_OK){
            return;
        }
        if(requestCode == REQUEST_DATE){
            Date date = (Date) data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            newDate = date;
            mDateButton.setText( new DateFormat().getLongDateFormat(this.getContext()).format(newDate) );
        }
    }



}
