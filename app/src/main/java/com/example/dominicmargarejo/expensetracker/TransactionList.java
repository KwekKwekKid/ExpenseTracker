package com.example.dominicmargarejo.expensetracker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerCursorWrapper;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbHelper;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.TransactionTable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Niko on 10/21/2017.
 *
 * List of transactions.
 */

public class TransactionList {

    private static TransactionList sTransactionList;

    private Context mContext;
    private SQLiteDatabase mDatabase;

    //Call this to get list.
    public static TransactionList get(Context context){
        if(sTransactionList == null){
            sTransactionList = new TransactionList(context);
        }
        return sTransactionList;
    }

    private TransactionList(Context context){
        mContext = context.getApplicationContext();
        mDatabase = new ExpenseTrackerDbHelper(mContext).getWritableDatabase();
    }

    public void addTransaction(Transaction transaction){
        ContentValues values = getContentValues(transaction);
        mDatabase.insert(TransactionTable.NAME, null, values);
    }

    public void updateTransaction(Transaction transaction){
        String uuidString = transaction.getId().toString();
        ContentValues values = getContentValues(transaction);
        mDatabase.update(TransactionTable.NAME, values,
                TransactionTable.Cols.UUID + " = ?",
                new String[] {uuidString});
    }

    public void removeTransaction(Transaction transaction){
        mDatabase.delete(TransactionTable.NAME, "uuid = ?", new String[]{transaction.getId().toString()});
    }

    public List<Transaction> getTransactions() {
        List<Transaction> transactions = new ArrayList<>();

        ExpenseTrackerCursorWrapper cursor = queryTransactions(null, null);

        try{
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                transactions.add(cursor.getTransaction());
                cursor.moveToNext();
            }
        } finally {
            cursor.close();
        }

        return transactions;
    }

    public Transaction getTransaction(UUID id){
        ExpenseTrackerCursorWrapper cursor = queryTransactions(
                TransactionTable.Cols.UUID + " = ?",
                new String[] {id.toString()}
        );

        try{
            if(cursor.getCount() == 0){
                return null;
            }
            cursor.moveToFirst();
            return cursor.getTransaction();
        } finally {
            cursor.close();
        }
    }

    public Transaction getRecentTransaction(String name){
        ExpenseTrackerCursorWrapper cursor = queryTransactions(
                TransactionTable.Cols.NAME + " = ?",
                new String[] {name}
        );

        try{
            if(cursor.getCount() == 0){
                return null;
            }
            cursor.moveToLast();
            return cursor.getTransaction();
        } finally {
            cursor.close();
        }
    }

    public int indexOf(Transaction transaction){
        ExpenseTrackerCursorWrapper cursor = queryTransactions(null,null);
        int index = 0;

        try{
            cursor.moveToFirst();
            do{
                Transaction tempTransaction = cursor.getTransaction();
                if(transaction.getId().equals(tempTransaction.getId())) {
                    break;
                }
            } while(cursor.moveToNext());

        } finally {
            index = cursor.getPosition();
            cursor.close();
        }

        return index;
    }

    private static ContentValues getContentValues(Transaction transaction){
        ContentValues values = new ContentValues();
        values.put(TransactionTable.Cols.UUID, transaction.getId().toString());
        values.put(TransactionTable.Cols.NAME, transaction.getTransactionName());
        values.put(TransactionTable.Cols.CATEGORY, transaction.getTransactionCategory());
        values.put(TransactionTable.Cols.AMOUNT, transaction.getTransactionAmount());
        values.put(TransactionTable.Cols.DATE, transaction.getTransactionDate().getTime());
        values.put(TransactionTable.Cols.ACCOUNT, transaction.getTransactionAccount());

        return values;
    }

    private ExpenseTrackerCursorWrapper queryTransactions(String whereClause, String[] whereArgs){
        Cursor cursor = mDatabase.query(
                TransactionTable.NAME,
                null, // columns - null = all columns
                whereClause,
                whereArgs,
                null, //groupBy
                null, // having
                null //orderBy
        );

        return new ExpenseTrackerCursorWrapper(cursor);
    }
}
