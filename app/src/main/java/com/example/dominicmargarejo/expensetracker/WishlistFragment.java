package com.example.dominicmargarejo.expensetracker;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbHelper;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class WishlistFragment extends Fragment {

    private static final String MODIFIED_POSITION = "modified_position";
    private static final int REQUEST_POSITION = 1;
    private int mModifiedPosition = 0;

    private RecyclerView mWishRecyclerView;
    private WishAdapter mAdapter;

    private List<Wish> wishes;

    private boolean FABExpanded;
    private FloatingActionButton mEditButton;
    private FloatingActionButton mAddButton;
    private FloatingActionButton mDeleteButton;

    public WishlistFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wishlist, container, false);

        mWishRecyclerView = (RecyclerView) view.findViewById(R.id.wishlist_recycler_view);
        mWishRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        FABExpanded = false;

        mAddButton = (FloatingActionButton) view.findViewById(R.id.add_wish_button);
        mAddButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mAddButton.setVisibility(View.GONE);
                mDeleteButton.setVisibility(View.GONE);
                Wish wish = new Wish();
                Wishlist.get(getContext()).addWish(wish);
                Intent intent = WishActivity.newIntent(getContext(), wish.getId());
                startActivityForResult(intent, REQUEST_POSITION);
            }
        });

        //doesn't really do anything yet
        mDeleteButton = (FloatingActionButton) view.findViewById(R.id.remove_wish_button);
        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mEditButton = (FloatingActionButton) view.findViewById(R.id.wishlist_open_actions_button);
        mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (FABExpanded){
                    mAddButton.setVisibility(View.GONE);
                    mDeleteButton.setVisibility(View.GONE);
                    FABExpanded = false;
                } else {
                    mAddButton.setVisibility(View.VISIBLE);
                    mDeleteButton.setVisibility(View.VISIBLE);
                    FABExpanded = true;
                }
            }
        });

        updateUI();

        return view;
    }

    private void updateUI() {
        Wishlist wishlist = Wishlist.get(getActivity());
        List<Wish> wishes = wishlist.getWishlist();

        if (mAdapter == null) {
            mAdapter = new WishAdapter(wishes,this);
            mWishRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setWishes(wishes);
            mAdapter.notifyItemChanged(mModifiedPosition);
        }
    }

    private class WishHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView mNameTextView;
        private TextView mCostTextView;
        private TextView mAccountTextView;
        private TextView mMaintainingTextView;
        private TextView mDateTextView;
        private TextView mProgressTextView;
        private TextView mBudgetTextView;
        private ProgressBar mProgressBar;
        private Wish mWish;

        private SQLiteDatabase mDatabase;

        private WishlistFragment mWishlistFragment;

        public WishHolder(View itemView, WishlistFragment wishlistFragment){
            super(itemView);
            mWishlistFragment = wishlistFragment;
            mNameTextView = (TextView) itemView.findViewById(R.id.list_item_wish_name_text_view);
            mCostTextView = (TextView) itemView.findViewById(R.id.list_item_wish_price);
            mAccountTextView = (TextView) itemView.findViewById(R.id.list_item_wish_account);
            mMaintainingTextView = (TextView) itemView.findViewById(R.id.list_item_wish_maintaining);
            mDateTextView = (TextView) itemView.findViewById(R.id.list_item_wish_date);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            mProgressTextView = (TextView) itemView.findViewById(R.id.progress_text);
            mBudgetTextView = (TextView) itemView.findViewById(R.id.wishlist_daily_budget);
            mDatabase = new ExpenseTrackerDbHelper(getActivity().getApplicationContext()).getWritableDatabase();

            itemView.setOnClickListener(this);
        }

        public void bindWish(Wish wish){
            mWish = wish;
            mNameTextView.setText(mWish.getWishName());

            double goal = mWish.getWishGoalAmount();
            mCostTextView.setText(String.format("\u20B1%.2f", goal));

            String accountName = mWish.getWishAccount();
            mAccountTextView.setText(accountName);

            double maintainingAmount = mWish.getWishMaintainingAmount();
            mMaintainingTextView.setText("(maintain " + String.format("\u20B1%.2f", maintainingAmount) + ")");

            Calendar wishDate = Calendar.getInstance();
            wishDate.setTime(mWish.getWishDate());

            long diffMs = wishDate.getTimeInMillis() - Calendar.getInstance().getTimeInMillis() ;
            long diffDays = TimeUnit.MILLISECONDS.toDays(diffMs);
            int daysLeft = Math.round(diffDays);
            if(daysLeft <= 0) {daysLeft = 0;}

            String dateText = new DateFormat().getLongDateFormat(getContext()).format(mWish.getWishDate());
            mDateTextView.setText(dateText + " (" + Integer.toString(daysLeft) + " days left)");

            Account account = AccountList.get(getContext()).getAccount(mWish.getWishAccount());
            int progress = 0;
            double accountBalance = 0;
            double dailyBudget = goal;
            if(account != null){
                accountBalance = account.getAccountInflow() - account.getAccountOutflow() - mWish.getWishMaintainingAmount();
                if(accountBalance<= 0) {
                    accountBalance = 0;
                }
                progress = (int) (accountBalance/goal*100);
                dailyBudget -= accountBalance;
                if(daysLeft > 0){ dailyBudget = Math.ceil( dailyBudget/daysLeft ); }

            }

            if(progress > 100) {progress = 100;}
            else if(progress < 0) {progress = 0;}

            if(progress == 100){
                dailyBudget = 0;
                daysLeft = 0;
                accountBalance = goal;
            }

            mBudgetTextView.setText( "Save \u20B1" + String.format("%.2f", dailyBudget) + " everyday for " + Integer.toString(daysLeft) + " days");

            mProgressBar.setProgress(progress);
            String progressText = Integer.toString(progress) + ".00% (\u20B1" + String.format("%.2f",accountBalance) + ")";
            mProgressTextView.setText(progressText);
        }

        @Override
        public void onClick(View v){
            Intent intent = WishActivity.newIntent(getActivity(), mWish.getId());
            startActivityForResult(intent, REQUEST_POSITION);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_POSITION){
            if(resultCode == Activity.RESULT_OK){
                mModifiedPosition = data.getIntExtra(MODIFIED_POSITION, 0);
            }
        }
    }

    private class WishAdapter extends RecyclerView.Adapter<WishHolder> {
        private List<Wish> mWishes;
        private WishlistFragment mWishlistFragment;

        public WishAdapter(List<Wish> wishes, WishlistFragment wishlistFragment){
            mWishes = wishes;
            mWishlistFragment = wishlistFragment;
        }

        @Override
        public WishHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_wishlist, parent, false);
            return new WishHolder(view, mWishlistFragment);
        }

        @Override
        public void onBindViewHolder(WishHolder holder, int position){
            Wish wish = mWishes.get(position);
            holder.bindWish(wish);
        }

        @Override
        public int getItemCount(){
            return mWishes.size();
        }

        public void setWishes(List<Wish> wishes){
            mWishes = wishes;
        }

    }

    @Override
    public void onResume(){
        super.onResume();
        updateUI();
    }

}
