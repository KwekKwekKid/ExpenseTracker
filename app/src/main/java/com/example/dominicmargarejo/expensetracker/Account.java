package com.example.dominicmargarejo.expensetracker;

import java.util.UUID;

/**
 * Created by Dominic Margarejo on 11/3/2017.
 *
 * Class for an account.
 * All transactions are assigned to an account.
 */

public class Account {
    private UUID mId; //This is not gonna be displayed, but it's for searching through the database.
    private String mAccountName = "";
    private double mAccountInflow = 0.00;
    private double mAccountOutflow = 0.00;

    public Account(){
        mId = UUID.randomUUID();
    }

    public Account(UUID id){
        mId = id;
    }

    public String getAccountName() {
        return mAccountName;
    }

    public void setAccountName(String AccountName) {
        mAccountName = AccountName;
    }

    public UUID getId() {
        return mId;
    }

    public void setId(UUID id) {
        mId = id;
    }

    public double getAccountInflow() {
        return mAccountInflow;
    }

    public void setAccountInflow(double accountInflow) {
        mAccountInflow = accountInflow;
    }

    public void addAccountInflow(double newInflow){
        mAccountInflow += newInflow;
    }

    public double getAccountOutflow() {
        return mAccountOutflow;
    }

    public void setAccountOutflow(double accountOutflow) {
        mAccountOutflow = accountOutflow;
    }

    public void addAccountOutflow(double newOutflow){
        mAccountOutflow += newOutflow;
    }
}
