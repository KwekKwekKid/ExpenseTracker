package com.example.dominicmargarejo.expensetracker.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import com.example.dominicmargarejo.expensetracker.Account;
import com.example.dominicmargarejo.expensetracker.Allowance;
import com.example.dominicmargarejo.expensetracker.Transaction;
import com.example.dominicmargarejo.expensetracker.Wish;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.AccountTable;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.TransactionTable;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.WishlistTable;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.AllowanceTable;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Niko on 11/4/2017.
 *
 * A cursor iterates through stuff in the database (I think).
 * This has extra functions to get instances of our custom classes like Transaction or Account.
 */

public class ExpenseTrackerCursorWrapper extends CursorWrapper {
    public ExpenseTrackerCursorWrapper(Cursor cursor){
        super(cursor);
    }

    public Transaction getTransaction(){
        String uuidString = getString(getColumnIndex(TransactionTable.Cols.UUID));
        String name = getString(getColumnIndex(TransactionTable.Cols.NAME));
        long date = getLong(getColumnIndex(TransactionTable.Cols.DATE));
        String category = getString(getColumnIndex(TransactionTable.Cols.CATEGORY));
        String account = getString(getColumnIndex(TransactionTable.Cols.ACCOUNT));
        double amount = getDouble(getColumnIndex(TransactionTable.Cols.AMOUNT));

        Transaction transaction = new Transaction(UUID.fromString(uuidString));
        transaction.setTransactionAmount(amount);
        transaction.setTransactionCategory(category);
        transaction.setTransactionAccount(account);
        transaction.setTransactionName(name);
        transaction.setTransactionDate(new Date(date));

        return transaction;
    }

    public Account getAccount(){
        String uuidString = getString(getColumnIndex(AccountTable.Cols.UUID));
        String name = getString(getColumnIndex(AccountTable.Cols.NAME));
        double inflow = getDouble(getColumnIndex(AccountTable.Cols.INFLOW));
        double outflow = getDouble(getColumnIndex(AccountTable.Cols.OUTFLOW));

        Account account = new Account(UUID.fromString(uuidString));
        account.setAccountName(name);
        account.setAccountInflow(inflow);
        account.setAccountOutflow(outflow);

        return account;
    }

    public Allowance getAllowance(){
        String uuidString = getString(getColumnIndex(AllowanceTable.Cols.UUID));
        String name = getString(getColumnIndex(AllowanceTable.Cols.NAME));
        double amount = getDouble(getColumnIndex(AllowanceTable.Cols.AMOUNT));
        String account = getString(getColumnIndex(AllowanceTable.Cols.ACCOUNT));
        int frequency = getInt(getColumnIndex(AllowanceTable.Cols.FREQUENCY));

        Allowance allowance = new Allowance(UUID.fromString(uuidString));
        allowance.setAllowanceName(name);
        allowance.setAllowanceAmount(amount);
        allowance.setAllowanceAccount(account);
        allowance.setAllowanceFrequency(frequency);

        return allowance;
    }

    public Wish getWish(){
        String uuidString = getString(getColumnIndex(WishlistTable.Cols.UUID));
        String name = getString(getColumnIndex(WishlistTable.Cols.NAME));
        double goal = getDouble(getColumnIndex(WishlistTable.Cols.PRICE));
        double maintaining = getDouble(getColumnIndex(WishlistTable.Cols.MAINTAINING));
        long date = getLong(getColumnIndex(WishlistTable.Cols.DATE));
        String account = getString(getColumnIndex(WishlistTable.Cols.ACCOUNT));

        Wish wish = new Wish(UUID.fromString(uuidString));
        wish.setWishName(name);
        wish.setWishGoalAmount(goal);
        wish.setWishMaintainingAmount(maintaining);
        wish.setWishDate(new Date(date));
        wish.setWishAccount(account);

        return wish;
    }

}
