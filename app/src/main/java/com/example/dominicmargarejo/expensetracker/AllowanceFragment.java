package com.example.dominicmargarejo.expensetracker;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerCursorWrapper;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbHelper;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema;
import com.example.dominicmargarejo.expensetracker.database.ExpenseTrackerDbSchema.AccountTable;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Dominic Margarejo on 12/7/2017.
 */

public class AllowanceFragment extends Fragment {
    private Allowance mAllowance;

    private EditText mNameField;
    private EditText mAmountField;

    private Spinner mFrequencySpinner;
    private Spinner mAccountSpinner;

    private Button mSubmitButton;
    private Button mCancelButton;


    //Used for determining whether to add or edit a allowance
    private boolean isNew;

    private String newName;
    private double newAmount;
    private int newFrequency;
    private String newAccount;

    private static final String ARG_ALLOWANCE_ID = "allowance_id";
    private static final String MODIFIED_POSITION = "modified_position";



    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        UUID allowanceId = (UUID) getArguments().getSerializable(ARG_ALLOWANCE_ID);

        mAllowance = AllowanceList.get(getActivity()).getAllowance(allowanceId);
        newName = mAllowance.getAllowanceName();
        newAmount = mAllowance.getAllowanceAmount();
        newAccount = mAllowance.getAllowanceAccount();
        newFrequency = mAllowance.getAllowanceFrequency();

        isNew = newName.equals("");

        if(isNew) { AllowanceList.get(getActivity()).removeAllowance(mAllowance); }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
    }

    //Used for determining which position in the list to update. I think. ???
    public void returnResult(int result){
        Intent data = new Intent();
        if(result != Activity.RESULT_CANCELED) {
            data.putExtra(MODIFIED_POSITION, AllowanceList.get(getActivity()).indexOf(mAllowance));
            System.out.println(AllowanceList.get(getActivity()).indexOf(mAllowance));
        }
        getActivity().setResult(result, data);
        getActivity().finish();
    }

    @Override
    public void onPause(){
        super.onPause();
        AllowanceList.get(getActivity()).updateAllowance(mAllowance);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_allowance, container, false);

        mNameField = (EditText) v.findViewById(R.id.allowance_name);
        mNameField.setText(mAllowance.getAllowanceName());
        mNameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                newName = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mAmountField = (EditText) v.findViewById(R.id.allowance_amount);
        mAmountField.setText(Double.toString(newAmount));
        mAmountField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (TextUtils.isEmpty(s)){
                    newAmount = 0.0;
                }
                else {
                    newAmount = Double.parseDouble(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mFrequencySpinner = (Spinner) v.findViewById(R.id.frequency_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.frequency_list, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mFrequencySpinner.setAdapter(adapter);
        mFrequencySpinner.setSelection(adapter.getPosition(Weekday.getDayInString(newFrequency)));
        mFrequencySpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id){
                newFrequency = Weekday.getDayInInt( parent.getItemAtPosition(pos).toString() );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){

            }
        });

        List<String> accounts = AccountList.get(getContext()).getAccountNames();
        mAccountSpinner = (Spinner) v.findViewById(R.id.account_spinner);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, accounts);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mAccountSpinner.setAdapter(adapter2);
        mAccountSpinner.setSelection(adapter2.getPosition(newAccount));
        mAccountSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id){
                newAccount = parent.getItemAtPosition(pos).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){

            }
        });

        mSubmitButton = (Button) v.findViewById(R.id.allowance_submit_button);
        mSubmitButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){


                if(newName.equals("")){
                    Toast.makeText(getContext(), R.string.allowance_nameless_error, Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    mAllowance.setAllowanceName(newName);
                    mAllowance.setAllowanceAmount(newAmount);

                    mAllowance.setAllowanceFrequency(newFrequency);


                    mAllowance.setAllowanceAccount(newAccount);

                    if(isNew){ AllowanceList.get(getActivity()).addAllowance(mAllowance); }
                }

                returnResult(Activity.RESULT_OK);
            }
        });

        mCancelButton = (Button) v.findViewById(R.id.allowance_cancel_button);
        mCancelButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                returnResult(Activity.RESULT_CANCELED);
            }
        });

        return v;
    }

    public static AllowanceFragment newInstance(UUID allowanceId){
        Bundle args = new Bundle();
        args.putSerializable(ARG_ALLOWANCE_ID, allowanceId);
        AllowanceFragment fragment = new AllowanceFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
