package com.example.dominicmargarejo.expensetracker;

import android.widget.ProgressBar;

import java.util.Date;
import java.util.UUID;

/**
 * Created by cherpanlilio on 11/3/17.
 */

public class WishlistItem {
    private UUID mId;
    private String mWishlistItemName;
    private double mWishlistAccAmount;
    private double mWishlistGoalAmount;
    private Date mWishlistDate;
    private ProgressBar mWishlistProgressBar;


    public WishlistItem(){
        mId = UUID.randomUUID();
        mWishlistDate = new Date();
    }

    public String getWishlistItemName() {
        return mWishlistItemName;
    }

    public void setWishlistItemName(String wishlistItemName) {
        mWishlistItemName = wishlistItemName;
    }

    public double getWishlistAccAmount() {
        return mWishlistAccAmount;
    }

    public void setWishlistAccAmount(double wishlistAccAmount) {
        mWishlistAccAmount = wishlistAccAmount;
    }

    public double getWishlistGoalAmount() {
        return mWishlistGoalAmount;
    }

    public void setWishlistGoalAmount(double wishlistGoalAmount) {
        mWishlistGoalAmount = wishlistGoalAmount;
    }

    public Date getWishlistDate() {
        return mWishlistDate;
    }

    public void setWishlistDate(Date wishlistDate) {
        mWishlistDate = wishlistDate;
    }

    public ProgressBar getWishlistProgressBar() {return mWishlistProgressBar;}

    public void setWishlistProgressBar(ProgressBar wishlistProgressBar){
        mWishlistProgressBar = wishlistProgressBar;
    }

    public UUID getId() {
        return mId;
    }

    public void setId(UUID id) {
        mId = id;
    }
}

