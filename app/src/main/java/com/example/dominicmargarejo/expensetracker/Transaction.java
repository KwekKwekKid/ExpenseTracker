package com.example.dominicmargarejo.expensetracker;

import java.util.Date;
import java.util.UUID;

/**
 * Created by Niko on 10/22/2017.
 *
 * Class for Transactions.
 * Stuff here should be self-explanatory.
 */

public class Transaction {
    private UUID mId;
    private String mTransactionName;
    private String mTransactionCategory;
    private double mTransactionAmount;
    private Date mTransactionDate;
    private String mTransactionAccount;

    public String getTransactionAccount() {
        return mTransactionAccount;
    }

    public void setTransactionAccount(String transactionAccount) {
        mTransactionAccount = transactionAccount;
    }

    public Transaction(){
        this(UUID.randomUUID());
        mTransactionName = "";
        mTransactionAccount = "";
        mTransactionAmount = 0.00;
        mTransactionCategory = "";
    }

    public Transaction(UUID id){
        mId = id;
        mTransactionDate= new Date();
        mTransactionName = "";
        mTransactionAccount = "";
        mTransactionAmount = 0.00;
        mTransactionCategory = "";
    }

    public String getTransactionName() {
        return mTransactionName;
    }

    public void setTransactionName(String transactionName) {
        mTransactionName = transactionName;
    }

    public String getTransactionCategory() {
        return mTransactionCategory;
    }

    public void setTransactionCategory(String transactionCategory) {
        mTransactionCategory = transactionCategory;
    }

    public double getTransactionAmount() {
        return mTransactionAmount;
    }

    public void setTransactionAmount(double transactionAmount) {
        mTransactionAmount = transactionAmount;
    }

    public Date getTransactionDate() {
        return mTransactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        mTransactionDate = transactionDate;
    }

    public UUID getId() {
        return mId;
    }

    public void setId(UUID id) {
        mId = id;
    }
}
