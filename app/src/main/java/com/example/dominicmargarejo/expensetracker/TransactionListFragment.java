package com.example.dominicmargarejo.expensetracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Niko on 10/21/2017.
 *
 * Fragment for the transaction list.
 */

public class TransactionListFragment extends Fragment {

    private static final String MODIFIED_POSITION = "modified_position";
    private static final int REQUEST_POSITION = 1;
    private boolean displayCheckboxes = false;
    private boolean deleteSelected = false;
    private int mModifiedPosition = 0;

    private RecyclerView mTransactionRecyclerView;
    private TransactionAdapter mAdapter;
    // Added This
    private List<Transaction> transactions;

    private boolean FABExpanded;
    private FloatingActionButton mEditButton;
    private FloatingActionButton mAddButton;
    private FloatingActionButton mDeleteButton;


    public void updateAdapter(){
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        // Added This
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_transaction_list, container, false);

        mTransactionRecyclerView = (RecyclerView) view.findViewById(R.id.transaction_recycler_view);
        mTransactionRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        FABExpanded = false;

        mEditButton = (FloatingActionButton) view.findViewById(R.id.open_actions_button);
        mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (FABExpanded){
                    mAddButton.setVisibility(View.GONE);
                    mDeleteButton.setVisibility(View.GONE);
                    FABExpanded = false;
                } else {
                    mAddButton.setVisibility(View.VISIBLE);
                    mDeleteButton.setVisibility(View.VISIBLE);
                    FABExpanded = true;
                }
            }
        });

        mAddButton = (FloatingActionButton) view.findViewById(R.id.add_transaction_button);
        mAddButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mAddButton.setVisibility(View.GONE);
                mDeleteButton.setVisibility(View.GONE);
                Transaction transaction = new Transaction();
                TransactionList.get(getContext()).addTransaction(transaction);
                Intent intent = TransactionActivity.newIntent(getContext(), transaction.getId());
                startActivityForResult(intent, REQUEST_POSITION);
            }
        });

        //doesn't really do anything yet
        mDeleteButton = (FloatingActionButton) view.findViewById(R.id.remove_transaction_button);
        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteSelected = true;
                updateAdapter();
                deleteSelected = false;

            }
        });

        updateUI();

        return view;
    }

    // Added This
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem item = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){

            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                newText = newText.toLowerCase();
                List<Transaction> temp = new ArrayList<>();
                for (Transaction a: transactions){
                    String name = a.getTransactionName().toLowerCase();
                    if (name.contains(newText)){
                        temp.add(a);
                    }
                }
                mAdapter.setTransactions(temp);
                updateAdapter();
                return true;
            }
        });
    }

    private void updateUI() {
        TransactionList transactionList = TransactionList.get(getActivity());
        transactions = transactionList.getTransactions();

        if (mAdapter == null) {
            mAdapter = new TransactionAdapter(transactions,this);
            mTransactionRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setTransactions(transactions);
            mAdapter.notifyItemChanged(mModifiedPosition);
        }
    }

    private class TransactionHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        private TextView mNameTextView;
        private TextView mDateTextView;
        private TextView mAmountPrefixTextView;
        private TextView mAmountTextView;
        private TextView mAccountTextView;
        private TextView mCategoryTextView;
        private Transaction mTransaction;
        private CheckBox mCheckBox;

        private TransactionListFragment mTransactionListFragment;

        public TransactionHolder(View itemView, TransactionListFragment transactionListFragment){
            super(itemView);

            mTransactionListFragment = transactionListFragment;

            mNameTextView = (TextView) itemView.findViewById(R.id.list_item_transaction_name_text_view);
            mDateTextView = (TextView) itemView.findViewById(R.id.list_item_transaction_date_text_view);
            mAmountPrefixTextView = (TextView) itemView.findViewById(R.id.list_item_amount_prefix_text_view);
            mAmountTextView = (TextView) itemView.findViewById(R.id.list_item_transaction_amount_text_view);
            mCategoryTextView = (TextView) itemView.findViewById(R.id.list_item_transaction_category_text_view);
            mAccountTextView = (TextView) itemView.findViewById(R.id.list_item_transaction_account_text_view);
            mCheckBox = (CheckBox) itemView.findViewById(R.id.list_item_checkbox);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        public void tickCheckBox(){
            if(!mCheckBox.isChecked()){
                mCheckBox.setChecked(true);
                mTransactionListFragment.displayCheckboxes = true;
                mCheckBox.setVisibility(CheckBox.VISIBLE);
            } else{
                mCheckBox.setChecked(false);
                mTransactionListFragment.displayCheckboxes = false;
            }
        }

        public void bindTransaction(Transaction transaction){
            mTransaction = transaction;
            mNameTextView.setText(mTransaction.getTransactionName());
            mDateTextView.setText(new DateFormat().getLongDateFormat(getContext()).format(mTransaction.getTransactionDate()));


            String amountPrefix = "+ \u20b1 ";
            if(mTransaction.getTransactionAmount() < 0){
                amountPrefix = "- \u20b1 ";
            }
            mAmountPrefixTextView.setText(amountPrefix);


            String amount = String.format("%.2f", Math.abs(mTransaction.getTransactionAmount()));
            mAmountTextView.setText(amount);

            mCategoryTextView.setText(mTransaction.getTransactionCategory());
            mAccountTextView.setText(mTransaction.getTransactionAccount());
        }

        @Override
        public void onClick(View v){
            Intent intent = TransactionActivity.newIntent(getActivity(), mTransaction.getId());
            startActivityForResult(intent, REQUEST_POSITION);
        }

        @Override
        public boolean onLongClick(View v){
            tickCheckBox();
            mTransactionListFragment.updateAdapter();
            return true;
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_POSITION){
            if(resultCode == Activity.RESULT_OK){
                mModifiedPosition = data.getIntExtra(MODIFIED_POSITION, 0);
            }
        }
    }

    private class TransactionAdapter extends RecyclerView.Adapter<TransactionHolder> {
        private List<Transaction> mTransactions;
        private TransactionListFragment mTransactionListFragment;

        public TransactionAdapter(List<Transaction> transactions, TransactionListFragment transactionListFragment){
            mTransactions = transactions;
            mTransactionListFragment = transactionListFragment;
        }

        @Override
        public TransactionHolder onCreateViewHolder(ViewGroup parent, int viewType){
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater.inflate(R.layout.list_item_transaction, parent, false);
            return new TransactionHolder(view, mTransactionListFragment);
        }

        @Override
        public void onBindViewHolder(TransactionHolder holder, int position){
            Transaction transaction = mTransactions.get(position);

            if(mTransactionListFragment.displayCheckboxes){
                holder.mCheckBox.setVisibility(CheckBox.VISIBLE);
                if(mTransactionListFragment.deleteSelected){
                    //INSERT CODE FOR DELETING
                }
            } else{
                holder.mCheckBox.setVisibility(CheckBox.GONE);
                holder.mCheckBox.setChecked(false);
            }

            holder.bindTransaction(transaction);

        }

        @Override
        public int getItemCount(){
            return mTransactions.size();
        }

        public void setTransactions(List<Transaction> transactions){
            mTransactions = transactions;
        }

    }

    @Override
    public void onResume(){
        super.onResume();
        if(displayCheckboxes){
            updateAdapter();
            displayCheckboxes = false;
        }
        updateUI();
    }
}
